// LNKLSTDBL.c [160929]
//
// Code provided for ELEC278
//
// This code implements a double linked list. It is derived from code in
// LNKLST1.
//
// This code has been deliberately written to reduce the number of asterisks.
// This is done by defining, via typedefs, types that are pointers to other
// types.  The student should compare code here with code in LNKLST1.c
// and decide which code convention - explicit pointers using asterisks or
// implicit pointers using pointer types - is more understandable.

#include <stdio.h>
#include <stdlib.h>

/* * START LICENSE

Code developed for educational purposes only.

Copyright 2016, 2017, 2018 by
David F. Athersych, Kingston, Ontario, Canada. (THE AUTHOR).
This software may be included in systems delivered or distributed by
Cynosure Computer Technologies Incorporated, Kingston, Ontario, Canada.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted, provided
that the above copyright notice appears in all copies and that both the
above copyright notice and this permission notice appear in supporting
documentation.  This software is made available "as is", and

THE AUTHOR DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, WITH REGARD
TO THIS SOFTWARE, INCLUDING WITHOUT LIMITATION ALL IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, AND IN NO EVENT
SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, TORT (INCLUDING NEGLIGENCE)
OR STRICT LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

Further, this software shall not be used in any life support systems,
autonomous vehicle systems, or flight control systems, without the
explicit written consent of the author.

For more information, see www.cynosurecomputer.ca

* END LICENSE */

//=============================================================================
// Node specific structure definition.  Node contaions actual data but also
// contains some structural information - pointers to the next and previous
// node.
//=============================================================================

// Node (or element) in list holds value and two pointers - one to next
// element and one to previous element.
// Notice the order of the typedef and the struct definition in this code.
// Also notice that a pNode is a pointer to a Node - which means there are
// fewer asterisks in the code.

typedef struct node	Node, *pNode;

struct node {
	pNode	next;		// pointer to node that follows this node
	pNode	prev;		// pointer to node that precedes this node
	int		key;		// key used to identify data item
	int		value;		// actual value of data
};
#define	SZ_NODE	(sizeof(struct node))


Node *CreateNode (int ky, int val, pNode nxt, pNode prv)
// Create new node structure and initialize fields.
// Returns pointer to new node created, or NULL if malloc() failed to
// find memory.
{
	pNode pnode = (pNode)malloc (SZ_NODE);
	if (pnode != NULL)	{
		pnode->next = nxt;
		pnode->prev = prv;
		pnode->key = ky;
		pnode->value = val;
		}
	return pnode;
}//createNode()


void DestroyNode (pNode pnode)
// Basically a pretty way of using free()
{
	free (pnode);
}//DestroyNode()


void PrintNode (pNode pnode)
// Node-specific display routine. Given pointer to node, it prints actual
// data.
{
	// always check to make sure parameter valid, because sometimes errors
	// happen.
	if (pnode != NULL)	{
		printf ("key: %d, value: %d\n", pnode->key, pnode->value);
	} else	{
		printf ("ERROR: PrintNode() - invalid Node pointer\n");
		}
}//PrintNode()

//=============================================================================
// End of node-specific code
//=============================================================================


//=============================================================================
// Linked-list specific code
//=============================================================================

// Linked List structure. In LNKLST0, there was no explicit list type - a list
// was defined by having a pointer to a first node.  Here, we have a specific
// list variable type and instance - which is separate from the list data to
// which it points.
// An instance of a struct lnkdlist is sometimes called a descriptor (as
// "linked list descriptor").

struct lnkdlist	{
	int		count;	// keep count of how many items in list
	pNode	llhed;	// point to first element in list
	pNode	llend;	// point to last element in list
};
typedef struct lnkdlist	LL, *pLL;
#define	SZ_LNKDLST		(sizeof(struct lnkdlist))


pLL CreateList (void)
// Create new linked list descriptor
{
	pLL	tmp;		// pointer to new linked list structure
	tmp = (pLL)malloc (SZ_LNKDLST);
					// get memory for lnkdlist structure
	if (tmp != NULL)	{
		// initialize fields in linked list descriptor
		tmp->count = 0;
		tmp->llhed = NULL;
		tmp->llend = NULL;
		}
	// Return pointer to initialized linked list structure or return NULL
	// if malloc() didn't get memory.
	return tmp;
}//CreateList()


void DestroyList(pLL plist)
// Destroy all memory used by linked list. This means destroying all nodes
// in linked list and then destroying linked list structure.
// Parameter is a copy of the pointer to the linked list structure.
{
	if (plist != NULL)	{
		pNode	tmp;			// used to traverse linked list
		tmp = plist->llhed;		// point to first node in linked list
		while (tmp != NULL)	{
			pNode	tmp2 = tmp->next;	// save pointer to next node in list
			DestroyNode (tmp);			// destroy current node
			tmp = tmp2;					// move to next node
			}
		// All nodes destroyed - destroy linked list structure
		free (plist);
		}
}//DestroyList()

// Note to student: The next code does have the asterisk ("pointer to")
// notation.  Make sure you understand why.

void DestroyListAndPointer(pLL *pplist)
// Destroy all memory used by linked list and zero pointer to linked list
// structure.  Parameter is a pointer to the pointer to the linked list.
{
	if (pplist != NULL)	{
		DestroyList (*pplist);	// use code above to destroy all content
								// of linked list, by passing copy of actual
								// list pointer to DestroyList().		
		*pplist = NULL;			// this will zero actual linked list
								// structure pointer, so there is now no
								// way to accidentally try to access
								// memory just free()ed.
		}
}//DestroyList()


//=============================================================================
// Linked List Utility Routines. Functions that operate on linked list itself,
// not content of the nodes.
//=============================================================================

int isEmptyList (pLL plist)
// Empty list detected either by count of 0 or by llhed==llend==NULL.
{
	return  (plist==NULL) || (plist->count == 0);
}//isEmptyList()


void PrintAllNodeData(pLL plist)
// Print all node data for Nodes found in list pointed to by plist.
{
	if (plist != NULL)	{
		pNode	thisnode;			// local pointer to nodes
		thisnode = plist->llhed;	// point to first node in list
		if (thisnode != NULL)	{
			// Data to print
			while (thisnode != NULL)	{
				PrintNode (thisnode);
				thisnode = thisnode->next;
				}
		} else	{
			printf ("EMPTY LIST\n");
			}
		}
}//PrintAllNodeData()


void PrintAllNodeDataReverse(pLL plist)
// NEW!
// Print all node data for Nodes found in list pointed to by plist in
// reverse order - i.e. from the end, not the beginning of the list
{
	if (plist != NULL)	{
		pNode	thisnode;			// local pointer to nodes
		thisnode = plist->llend;	// point to last node in list
		if (thisnode != NULL)	{
			// Data to print
			while (thisnode != NULL)	{
				PrintNode (thisnode);
				thisnode = thisnode->prev;
				}
		} else	{
			printf ("EMPTY LIST\n");
			}
		}
}//PrintAllNodeDataReverse()


//=============================================================================
// Functions to manage list - adding and removing nodes.  You may note
// that all code above actually hasn't changed interface from the single link
// version. The implementation has changed.
// (Well, except for PrintAllNodeDataReverse() which wasn't feasible
// in the single link version.)
//=============================================================================


void AddNodeToFront(pLL plist, pNode pnewnode)
// Code to add new node to front of existing list.
// Parameters:
//	plist - points to existing linked list
//	newnode - points to new node which is to be added (see createNode())
{
	if (plist != NULL && pnewnode != NULL)	{
		Node	*tmp;			// temporary pointer to first list node
		tmp = plist->llhed;		// point to current first node (may be NULL)
		// Node pointed to by newnode will become first node in list. Existing
		// list may have Nodes,in which case head of list points to first one.
		// Existing list may be empty, meaning head of list contains NULL.
		pnewnode->next = tmp;	// New node points to existing list
		plist->llhed = pnewnode;// newnode becomes first in list
		// Check if this was first addition to an empty list
		if (tmp == NULL)	{
			// original list was empty, so we are adding first node.  It also
			// becomes last node.
			plist->llend = pnewnode;
		} else	{
			// Not the first addition - there were nodes before. Note that
			// the original first node is pointed to by tmp.  That node's
			// previous pointer needs to be updated.
			tmp->prev = pnewnode;
			}//endif empty/nonempty list
		}
}//AddNodeToFront()	


void AddNodeToEnd (pLL plist, pNode pnewnode)
// Add new node to end of existing list. 
{
	if (plist != NULL  && pnewnode != NULL)	{
		pNode	tmp;				// temporary pointer to last node in list
		tmp = plist->llend;			// point to last in current list
		plist->llend = pnewnode;	// new node becomes new last node

		if (tmp == NULL)	{
			// this is case where list is empty - new node alse becomes
			// first node
			plist->llhed = pnewnode;
		} else	{
			// this is case where list has at least one element - and thus
			// has a last element that tmp points to
			tmp->next = pnewnode;	// current last node points to new node
			pnewnode->prev = tmp;	// new last node points to 
			}
		}
}//AddNodeToEnd()


void RemoveNodeFromFront (pLL plist)
// Remove first node from list
{
	if (plist != NULL)	{
		pNode	tmp;		// temporary used to point to node to delete
		tmp = plist->llhed;	// point to first node (or perhaps NULL)
		if (tmp != NULL)	{
			// list not empty, so there is a first node to delete
			pNode	tmp2;
			tmp2 = tmp->next;		// pointer to second node in list
			plist->llhed = tmp2;	// which now becomes first node
			if (tmp2 == NULL)	{
				// There was no second node - only only the first node
				// and we just unlinked it.  Update endlist pointer.
				plist->llend = NULL;
			} else	{
				// There was a second node and its previous pointer is
				// pointing to the node we're deleting.  Fix this.
				tmp2->prev = NULL;
				}
			// Node pointed to by tmp, the former first node, is no
			// longer part of list
			DestroyNode (tmp);
			}//endif non-empty list
		}//endif valid list pointer
}//RemoveNodeFromFront()


void RemoveNodeFromEnd (pLL plist)
// Remove last node from list. 
{
	if (plist != NULL)	{
		pNode	tmp;		// temporary used to point to node to delete
		tmp = plist->llend;	// point to last node (or perhaps NULL)
		if (tmp != NULL)	{
			// list not empty, so there is a last node to delete
			pNode	tmp2;
			tmp2 = tmp->prev;	// pointer to penultimate node in list
			plist->llend = tmp2;// which now becomes last node
			if (tmp2 == NULL)	{
				// There was no second-to-last node, only a last node, and
				// it has just been unlinked. Need to update head pointer.
				plist->llhed = NULL;
			} else	{
				// There was a second-to-last node and its next pointer is
				// pointing to the node we're deleting.  Fix this.
				tmp2->next = NULL;
				}
			// node pointed to by tmp, the former last node, is no longer
			// necessary.
			DestroyNode (tmp);
			}//endif non-empty list
		}//endif valid list pointer
}//RemoveNodeFromEnd()


int main ()
{
	pLL		linklist1;			// pointer to a linked list
	int		i;
	pNode	ptmp;

	linklist1 = CreateList ();	// now we have a linked list descriptor
	PrintAllNodeData (linklist1);
	printf ("ADD 1 NODE\n");
	ptmp = CreateNode (1, 11, NULL,NULL);
	AddNodeToFront (linklist1, ptmp);
	PrintAllNodeData (linklist1);

	printf ("ADD (2,12) THEN (3,13) TO FRONT\n");
	AddNodeToFront (linklist1, CreateNode (2, 12, NULL, NULL));
	AddNodeToFront (linklist1, CreateNode (3, 13, NULL, NULL));
	PrintAllNodeData (linklist1);

	printf ("ADD (4,14) THEN (5,15) TO END\n");
	AddNodeToEnd (linklist1, CreateNode (4, 14, NULL, NULL));
	AddNodeToEnd (linklist1, CreateNode (5, 15, NULL, NULL));
	PrintAllNodeData (linklist1);

	printf ("DELETE FIRST NODE\n");
	RemoveNodeFromFront (linklist1);
	PrintAllNodeData (linklist1);

	printf ("DELETE LAST NODE\n");
	RemoveNodeFromEnd (linklist1);
	PrintAllNodeData (linklist1);
	
	DestroyList(linklist1);
    return 0;
}
