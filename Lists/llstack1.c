// LLSTACK1.C [161002]
// Simple implementation of an integer stack using linked list.
// Recall behavior of a stack - an item can be pushed onto the stack,
// and an item can be popped from the stack. Our definition of stack
// also allows inspection of the item at the top of the stack.
// Thinking about this, it seems that a linked list, where all the
// additions and deletions only take place at the front of the list,
// would work.
//
// This implementation explicitly shows the linked list
// implementation separate from the stack implementation.

#include <stdio.h>


enum _Result {FAILURE=0, SUCCESS};
typedef enum _Result	Result;

// Linked list of integers supporting only addfront and delete front

typedef struct _n {
	struct _n	*next;
	int			nodval;
} Node;

#define	SZNODE	(sizeof(struct _n))

// Pointer to first node in list
Node		*head = NULL;

Result AddFront (int n)
// Add value n to front of list. Return 0 if failure, non-zero
// otherwise.
{
	Node *p = (Node *)malloc(SZNODE);
	if (p == NULL)	return FAILURE;
	p->nodval = n;
	p->next = head;		// new node points to existing list
	head = p;			// head updated to point to new node
	return  SUCCESS;
}//AddFront()

Result DeleteFront (int *pvalue)
// Delete first element in list. If parameter pvalue is not NULL,
// copies value in first element to integer pointed to by pvalue
// before deleting node.
// Returns 0 if list empty, 1 if a Node deleted
{
	Node *p = head;
	if (p == NULL)  return FAILURE;	// empty list - cannot delete
	head = p->next;					// remove node from list
	// did caller want value in first node? If so, give it
	if (pvalue)	*pvalue = p->nodval;
	free (p);						// get rid of node
	return  SUCCESS;
}//DeleteFront()


// Stack implemented explicitly using linked list

Result push (int n)
{
	return AddFront(n);
}//push()


Result  pop (int *pvalue)
{
	return DeleteFront (pvalue);	
}// pop()

        
Result  tos (int *pvalue)
{
	Result	r;
	// Take front item off list, retrieving value
	r = DeleteFront (pvalue);
	if (r == SUCCESS)	{
		// put value back at front of list
		r = AddFront (*pvalue);
		}
	return r;
} // tos()


int main (void)
{
	int		i;
	int		rslt;
	int		value;
	int		*pi;

	// Push values 20, 21, ..., 24 onto stack
	for (i=0; i<5; i++)	push (20+i);

	// Now pop everything off - should display last one pushed first
	printf ("Popping values off stack and showing in pop order\n   ");
	while (rslt = pop (&value))	printf ("  %2d", value);
	putchar ('\n');

	for (i=0; i<5; i++)
		push (100+i);
	while (rslt = pop (&value))	{
		printf ("  %2d", value);
		}
	putchar ('\n');

	return 0;
}
		


