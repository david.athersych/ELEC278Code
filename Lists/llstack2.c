// LLSTACK.C [161002]
// Simple implementation of an integer stack using linked list.
// Recall behavior of a stack - an item can be pushed onto the stack,
// and an item can be popped from the stack. Our definition of stack
// also allows inspection of the item at the top of the stack.
// Thinking about this, it seems that a linked list, where all the
// additions and deletions only take place at the front of the list,
// would work.
//
// This implementation does not explicitly show the linked list
// implementation separate from the stack implementation.

#include <stdio.h>

typedef struct _sn {
	struct _sn	*next;
	int			nodval;
} StkNode;

#define	SZSTKNODE	(sizeof(struct _sn))

StkNode		*head = NULL;




int push (int n)
{
	StkNode *p = (StkNode *)malloc(SZSTKNODE);
	if (p == NULL)	return 0;
	p->nodval = n;
	p->next = head;
	head = p;
	return  1;
}//push()


int  pop (int *value)
{
	StkNode *p = head;
	if (p == NULL)  return 0;
	head = p->next;
	*value = p->nodval;
	free (p);
	return  1;
}// pop()

        
int  tos (int **ptop)
{
	StkNode *p = head;
	if (p == NULL) return 0;
	*ptop = &(p->nodval);
	return  1;
} // tos()


int main (void)
{
	int		i;
	int		rslt;
	int		value;
	int		*pi;

	for (i=0; i<5; i++)
		push (20+i);

	while (rslt = pop (&value))
		printf ("  %2d", value);
	putchar ('\n');

	for (i=0; i<5; i++)
		push (100+i);
	while (rslt = pop (&value))	{
		printf ("  %2d", value);
		rslt = tos (&pi);
		*pi = *pi +50;
		}
	putchar ('\n');

	return 0;
}
		


