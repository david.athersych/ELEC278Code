--Inserting value 30 into AVL tree
--Inserting value 25 into AVL tree
  Insert 25 into left subtree of 30
--Inserting value 35 into AVL tree
  Insert 35 into right subtree of 30
--Inserting value 20 into AVL tree
  Insert 20 into left subtree of 30
  Insert 20 into left subtree of 25
--Inserting value 40 into AVL tree
  Insert 40 into right subtree of 30
  Insert 40 into right subtree of 35
--Inserting value 21 into AVL tree
  Insert 21 into left subtree of 30
  Insert 21 into left subtree of 25
  Insert 21 into right subtree of 20
	Have to repair imbalance
	Left rotate around: 20
	Right rotate around: 25
--Inserting value 50 into AVL tree
  Insert 50 into right subtree of 30
  Insert 50 into right subtree of 35
  Insert 50 into right subtree of 40
	Have to repair imbalance
	Left rotate around: 35
--Inserting value 5 into AVL tree
  Insert 5 into left subtree of 30
  Insert 5 into left subtree of 21
  Insert 5 into left subtree of 20
--Inserting value 60 into AVL tree
  Insert 60 into right subtree of 30
  Insert 60 into right subtree of 40
  Insert 60 into right subtree of 50
--Inserting value 19 into AVL tree
  Insert 19 into left subtree of 30
  Insert 19 into left subtree of 21
  Insert 19 into left subtree of 20
  Insert 19 into right subtree of 5
	Have to repair imbalance
	Left rotate around: 5
	Right rotate around: 20
--Inserting value 18 into AVL tree
  Insert 18 into left subtree of 30
  Insert 18 into left subtree of 21
  Insert 18 into left subtree of 19
  Insert 18 into right subtree of 5
	Have to repair imbalance
	Right rotate around: 21
--Inserting value 17 into AVL tree
  Insert 17 into left subtree of 30
  Insert 17 into left subtree of 19
  Insert 17 into right subtree of 5
  Insert 17 into left subtree of 18
	Have to repair imbalance
	Right rotate around: 18
	Left rotate around: 5
--Inserting value 15 into AVL tree
  Insert 15 into left subtree of 30
  Insert 15 into left subtree of 19
  Insert 15 into left subtree of 17
  Insert 15 into right subtree of 5
--Done inserting value 15 into AVL tree

