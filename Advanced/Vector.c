// VECTOR.C
// Code to implement vector (resizable array) in C.
// Vector implements indexable data collection that grows as necessary.
//
// Source code used for teaching purposes in course ELEC278,
// Queen's University, Kingston, Winter semester 2018.
//
// Note that is code has some limitations.  It is suggested that
// students think about improving this code.
//
// This code is provided without warranty of any kind. It is the
// responsibility of the user to determine the correctness and the
// usefulness of this code.
//
// Author:  David F. Athersych, P.Eng.
//          Cynosure Computer Technologies Incorporated
// All rights reserved.
//
// See LICENCE.MD for restrictions on the use of this code.
//
// History:
//	910315	DFA	1.0	QNX Shell version 0.9
//	910428	DFA	1.1	QNX Shell version 0.91
//	910719	DFA	1.2	QNX Shell version 1.0
//	910930	DFA	2.0	Added to QNX library
//	920210	DFA	3.0	MSDOS Library version
//	181027	DFA	4.0	Updated version for ELEC278


#include	<stdio.h>

typedef	unsigned char	Byte;

// Vector structure implementation
struct _vector	{
	Byte	*a;			// pointer to array that will hold actual data
	int		unitsize;	// size (in bytes) of one vector element
	int		allocsize;	// current size of allocated storage (in bytes)
	int		maxindex;	// convenience - essentially (allocsize/unitsize)-1
};
#define	VectorSZ	(sizeof(struct _vector))
typedef struct _vector	Vector, *pVector;

// The following variable is used to control the printing of various
// debugging information. Set to non-zero for lots of output.
static int	flg_verbose = 0;


// Next two routines are essentially destructor and constructor for
// vector. 

void  vectordestroy (pVector *ppv)
// Remove instance of Vector.
// Danger! This code accepts pointer to vector, but does nothing to
// ensure that pointer to vector is cleared.  That means some badly
// written code could destroy a vector, then try to continue to access
// vector memory. An exercise for student would be to eliminate this
// issue.
{
	// make sure vector pointer has some possibility of being valid
    if (ppv != NULL)	{
		Vector	*pv = *ppv;
		// OK, minimal test has determined that pointer points to
		// something.
		// If first field is valid pointer, free memory it points to.
    	if (pv->a != NULL) free (pv->a);
		// Now free vector descriptor
    	free (ppv);
		}
	return;
}//vectordestroy()


static int growstorage (Vector *pv)
// Internal routine to grow internal storage allocated for vector.
{
	int		rslt = -1;
	// Check for valid vector descriptor.
	// Boss: Dave - this is a static function! It can only be called
	// from some of your other functions in this module and you never
	// make mistakes! You don't need this check!
	// Dave: That's true, I don't make mistakes, but sooner or later
	// someone else is going to edit this code, and I don't have any
	// confidence in them!
	if (pv != NULL)	{
		printf ("--Growing data storage from %d to %d bytes\n",
					pv->allocsize, (pv->allocsize==0 ? 256*pv->unitsize :
														pv->allocsize*2));
		if (pv->a == NULL)	{
			// No storage currently allocated.  Make guess for initial
			// size based on element size.
			int		numberunits = (pv->unitsize > 128) ? 64 : 256;
			int		bytes_reqd = numberunits * pv->unitsize;
			pv->a = (Byte *)malloc (bytes_reqd);
			if (pv->a != NULL)	{
				// got memory
				pv->allocsize = bytes_reqd;
				pv->maxindex = numberunits-1;
				rslt = 0;
				}
		} else	{
			// Already have memory allocated, but seem to need more.
			// Employ simple strategy of doubling allocated space.
			int		bytes_reqd = pv->allocsize * 2;
			Byte	*anew = (Byte *)realloc(pv->a, bytes_reqd);
			if (anew != NULL)	{
				pv->a = anew;
				pv->allocsize = bytes_reqd;
				pv->maxindex += pv->maxindex + 1;
				rslt = 0;
				}
			}
		}
	return rslt;
} //growstorage()
			

Vector* vectorcreate (
			int		unitsz,		// size, in bytes, of vector element
			int		startsize)	// suggested starting number of elements
// Creates new vector. Size of vector element must be provided.  Initial
// count of elements can be 0 - just means first write access will allocate
// memory.  Returns NULL if there is an error - bad parameter or malloc()
// failure; otherwise returns pointer to new vector instance.
{
	pVector	rslt	= NULL;		// default return value

	if (unitsz > 0)	{
		// user read the instructions.
		rslt = (pVector) malloc (VectorSZ);
		if (rslt != NULL)	{
			// Got vector descriptor. Set up unitsize
			rslt->unitsize = unitsz;
			if (flg_verbose)
				printf ("--Create vector, unitsize: %d  --", unitsz);
			// Did user want initial space allocated?
			if (startsize < 1)	{
				// No.
				rslt->a = NULL;
				rslt->maxindex = -1;
				rslt->allocsize = 0;
				printf (" No initial space allocated\n");
			} else	{
				// Yes. User thinks they have some insight.
				int		bytes_reqd = unitsz * startsize;
				if (flg_verbose)
					printf (" Initial space: %d bytes (%d units)\n",
							bytes_reqd, startsize);
				rslt->a = (Byte *)malloc (bytes_reqd);
				if (rslt->a == NULL)	{
					// oops - no memory? Then no vector!
					free (rslt);
					rslt = NULL;
				} else	{
					rslt->allocsize = bytes_reqd;
					rslt->maxindex = startsize-1;
					}
				}
			}
		}//endif (unitsz >0)
	return rslt;
}//vectorcreate()


Byte *pointtoelement (pVector pv, int index)
// Return pointer to an element in vector pv. Returns NULL if index (or
// something else) not valid, otherwise it returns pointer to index 
// position in vector.  Note that this is a dangerous routine - if
// array has to be moved (as result of realloc()), pointer may no longer
// be valid.  So, use pointer before doing store() and always reload
// pointers after doing store().
// Exercise for the student: How would you fix this?
//
// Note - this routine returns a Byte pointer to some place in vector.
// The pointer is aligned to an element-sized place in the vector. Caller
// needs to cast appropriately!
//
{
	// check obvious errors first
    if (pv == NULL || index < 0) return NULL;
    // check for index exceeding upper bound
    if (index >= pv->maxindex) return NULL;

    // everything appears in order - figure out offset in Byte array for
	// index element
    return &(pv->a[index * pv->unitsize]);
}//pointtoelement()


int get_element (pVector pv, int index, void *pdata)
// Get data value stored at position index in vector. We don't know
// what types elements are, so we do straight byte-for-byte copy.
{
	Byte	*src;
	int		rslt = -1;				// default return value
	// check obvious errors first
    if (pv != NULL && index >= 0 && index <= pv->maxindex)	{
    	// everything appears in order - figure out pointer to location in
		// vector.
    	src = pv->a + index*(pv->unitsize);
		// copy element
		if (flg_verbose)
			printf ("Address of index %d:  %012x\n", index, src);
		memcpy (pdata, src, pv->unitsize);
		rslt = 0;
		}
	return rslt;
}//getelement()


int set_element (Vector *pv, int index, void *pdata)
// Store value into index position of Vector pv.  Note that caller's data is
// referenced by pointer, because we don't know the type and hence we
// do not know how big it is.
// Returns 1 if store was successful; 0 if something went wrong.
{
	int rslt;
    // check for obvious errors first
    if (pv == NULL || pdata == NULL || index < 0)  rslt = -1;
	else	{
		Byte	*pdst;
		int		*tmp = (int *)pdata;
		// Check if index exceeds current array size.  This is coded as a
		// loop, because the element being written may not be in the range
		// of the last enlargement.
		// Note this loop is not written with any safety checks - it could
		// repeat until all memory has been exhausted.
		while (index > pv->maxindex)	{
			if (growstorage (pv))	{
				// Tried to enlarge storage, but effort failed.
				return -1;
				}
			}//endwhile
		// Storage is now large enough for index specified
		pdst = pv->a + index*(pv->unitsize);
		if (flg_verbose)
			printf ("--Load into offset %d [%X]  value %d\n", index*(pv->unitsize),
					pdst, *tmp);
		// copy element
		memcpy (pdst, pdata, pv->unitsize);
		rslt = 0;
		}
	return rslt;
}//store()


#ifdef TESTVECTOR

// main() routine just performs some tests on the vector code

int  main (void)
{
	int		i, val;
	int		tmp;
	int		tmpch;
	Vector	*v = vectorcreate (sizeof (int), 0);

    for (i=0; i<40; i++)	{
		tmp = i+370;
		printf ("Storing %d in index %d\n", tmp, i);
		set_element (v, i, (void *)&tmp);
		//tmpch = getchar();
		}
    for (i=0; i<40; i++)  {
		printf ("%3d: ", i);
        if (get_element (v, i, (void *)&tmp))   
			printf (" NA\n");
		else
			printf (" %d\n", tmp);
        }
#if 1
    for (i=300; i<320; i++)	{
		tmp = i +1000;
		printf ("Storing %d in index %d\n", tmp, i);
		set_element (v, i, (void *)&tmp);
		}
    for (i=300; i<320; i++)  {
		printf ("%3d: ", i);
        if (get_element (v, i, (void *)&tmp))   
			printf (" NA\n");
		else
			printf (" %d\n", tmp);
        }
#endif
    return 0;
}
#endif
