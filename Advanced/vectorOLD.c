// VECTOR.C
// Code to implement vector in C.
// Vector implements indexable data collection that grows as necessary.


// Editorial note for ELEC278:
// Some of you wanted to see how a vector might be implemented.  I described
// a vector as an unspecified size collection of items, accessed by an index.
// I described an array as a specified size collection of items, accessed by
// an index. Arrays are fundamental; vectors a little more abstract.  In
// general, abstract things have to be implemented with real things, so vectors
// are implemented with arrays, and some code that knows when to go get more
// array when needed.
//


#include	<stdio.h>

// The following is a (crude) way of getting a template class in a language
// without classes or templates. I declare a new type T, and use that for all
// references to data in the declarations and implementation of my data
// structure.  The idea is if I wanted vectors of floats, I could just
// replace the next line with:
//    typedef float T;
// Or if I wanted a vector of some structure, I could use:
//    typedef struct mystruct  T;
// This is the reason that I used a pointer to a data item instead of the
// data item itself in the store routine.


typedef	int	T;             // Create vector storage-type agnostic.



// Vector structure implementation
struct _vector	{
	T  *a;                 // pointer to array that will hold actual heap data
    int	arraysize;         // size of storage - i.e. index of first element off
                           // end of array
    int vectorsize;        // Size that user requested (either initially or
                           // through usage
};
typedef struct _vector   Vector;


// Next two defines have to synchronize - all array block sizes will be power
// of 2.  Number of blocks needed is 1 + requested size divided by BLKSIZE.
// (Remember that the index is being used to determine number of blocks - the
// number of elements is actually 1 more than the (0-based) index.

#define BLKORDER     5
// BLKSIZE defined to be 1 shifted by BLKORDER bits. Shift by 0 means BLKSIZE
// is 1; by 1, BLKSIZE 2; by 2, BLKSIZE 4; by 3, BLKSIZE 8; etc.
#define BLKSIZE      (1<<BLKORDER)
// Block count is the number of blocks of size BLKSIZE needed to hold a
// collection of s items.  BLKSIZE*BLKCOUNT(s) >= s
#define BLKCOUNT(s)  ((s+BLKSIZE) >> BLKORDER)

#define debug 0      // can be overridden by compiler option


// Next two routines can be considered the destructor and the constructor of
// an instance of a vector.  

void  vectordestroy (Vector *pv)
// Remove instance of Vector.  Note that pointer to Vector has been set to
// NULL so there is no danger that caller will reuse it.
{
    if (pv == NULL)    return;
    if (pv->a != NULL) free (pv->a);
    free (pv);
	pv = NULL;
}//vectordestroy()


Vector*  vectorinit (int size)
// Create vector instance.  If size specified, allocate enough memory to hold
// vector of specified size.  Return pointer to vector instance; NULL if
// vector could not be created.
{
    Vector    *pv = (Vector *)malloc (sizeof(struct _vector));
    // Did we get some memory for vector description?
    if (pv == NULL)    return NULL;

    // Initialization of vector depends on whether size specified
    if (size <= 0)    {
        // caller did not specify size. Start with no space.
        pv->a = NULL;
        pv->arraysize = 0;
        pv->vectorsize = 0;
    } else  {
        // caller specified vector size.  We make some adjustments.
        // First, compute number of BLKSIZE element blocks needed to hold
        // caller's size vector.
        int arraysize = BLKSIZE*BLKCOUNT(size);
        pv->a = (T *) malloc (arraysize * sizeof (T));
        if (pv->a == NULL)   {
            // Didn't get memory to store data - not much else we can do
			// Abandon vector descriptor.
            free (pv);
            return NULL;
            }
        // user gets vector of size requested and we remember how much
        // we really have.
        pv->vectorsize = size;
        pv->arraysize = arraysize;
        }
    // Set up vector with or without initial memory
    return pv;
}//vectorinit()


int at (Vector *pv, int index, T* pdata)
// Access index element of vector pv, storing result in user's data element.
// That is, access element >at< index in vector pv, hence the name.
// Returns 1 if index was valid - and returns data in place pointed to by pdata;
// returns 0 if index was out of bounds and doesn't modify user's data element
{
    // check obvious errors first
    if (pv == NULL || pdata == NULL || index < 0) return 0;
    // check for index exceeding upper bound
    if (index >= pv->vectorsize) return 0;

    // everything appears in order
    *pdata = pv->a[index];
    return 1;
}//at()


T* pointto (Vector *pv, int index)
// Return pointer to an element in vector pv. Returns NULL if index (or
// something else) not valid, otherwise it returns pointer to index 
// position in vector.  Note that this is a dangerous routine - if
// array has to be moved (as result of realloc()), pointer may no longer
// be valid.  So, use pointer before doing store() and always reload
// pointers after doing store().
//
// Exercise for the student: How would you fix this?
{
	// check obvious errors first
    if (pv == NULL || index < 0) return NULL;
    // check for index exceeding upper bound
    if (index >= pv->vectorsize) return NULL;

    // everything appears in order
    return &pv->a[index];
}//pointto()


int store (Vector *pv, int index, T* pdata)
// Store value into index position of Vector pv.  Note that caller's data is
// referenced by pointer, just in case it is actually some large data
// structure.
// Returns 1 if store was successful; 0 if something went wrong.
//
// Note that this doesn't mean we just keep a copy of a pointer to
// the caller's data; in the last line, the actual data is copied.
{
    // check for obvious errors first
    if (pv == NULL || pdata == NULL || index < 0)  return 0;

    // Check if index exceeds current arraysize
    if (index >= pv->arraysize)   {
        // Index is bigger than actual memory available - have to get more,
        // while preserving data already stored.
        int newarraysize = BLKSIZE*BLKCOUNT(index);
        T* pnewa = (T *)realloc (pv->a, newarraysize * sizeof (T));
        if (debug)
            printf ("About to resize: index = %d,  newarraysize = %d\n",
                     index, newarraysize);
        // Didn't get more memory - at least what we have is still OK
        if (pnewa == NULL)    return 0;

        // We have enlarged storage space
        pv->arraysize = newarraysize;
        pv->a = pnewa;
        }
     // Check if index exceeds currect vectorsize.  We are sure that it doesn't
     // exceed arraysize.
     if (index >= pv->vectorsize)  {
        // Update vectorsize
        pv->vectorsize = index+1;  // i.e. index is highest valid index
        }
     // OK - vectorsize and arraysize reflect fact that we're trying to insert
     // at given index position.  Let's do this.
     pv->a[index] = *pdata;
     return 1;
}//store()


int shorten (Vector *pv, int newlength)
// Shorten the vector. This means that available part of vector is shorter, but
// it doesn't mean that array that holds vector is shorter. In fact, this
// is a lazy implementation, in that it does not reduce the arraysize ever.
{
    if (pv == NULL || newlength < 0)    return 0;
    // Cannot use shorten to make vector longer!!
    if (newlength > pv->vectorsize)     return 0;
    pv->vectorsize = newlength;
    return 1;
}//shorten()


#define TEST

#ifdef TEST

// main() routine just performs some tests on the vector code

int  main (void)
{
    int      i;
    int      tmp;
    Vector   *v = vectorinit (0);


    for (i=0; i<40; i++)    store (v, i, &i);

    for (i=0; i<42; i++)  {
        if (access (v, i, &tmp))    printf (" %d", tmp);
        else                        printf (" NA");
        }
    printf ("\n");

    shorten (v, 30);

    for (i=0; i<42; i++)  {
        if (access (v, i, &tmp))    printf (" %d", tmp);
        else                        printf (" NA");
        }
    printf ("\n");
    return 0;
}
#endif
