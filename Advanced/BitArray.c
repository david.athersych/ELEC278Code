// BITARRAY.C [161121]
//
// Simple code to show creation and manipulation of bit arrays.
//
// Author: David F.Athersych, P.Eng.
//         Cynosure Computer Technologies Incorporated
//

#include <stdio.h>
#include <stdlib.h>

#define	WORDSZ	32				// Bits in an unsigned integer

// When SHOW set to 1, code prints messages indicating what it is doing
#define	SHOW	1

typedef struct _bitarray {
	int				size;		// number of bits in array
	unsigned int	bits[1];	// actual storage for bits (size is fake)
} BitArray, *pBitArray;


void destroybitarray (pBitArray p)
{
	if (p != NULL)
		free (p);
}


pBitArray initbitarray (int size)
// Create bitarray to hold (at least) size bits
{
	int			ints_needed, k;
	pBitArray	p;

	if (size <= 0)	return NULL;

	// Figure out how many ints needed - assume 32 bit integer
	ints_needed = (size/WORDSZ)+1;
	if (SHOW)
		printf ("Size = %d   Ints needed = %d\n", size, ints_needed);

	// alternately, could figure out how many bits in an int by taking
	// size in bytes and multiplying by 8 (shifting 3 bit positions left).
	// ints_needed = (size / ((sizeof (int))<<3)) + 1;

	// Get memory for bit array - we calculated how many ints we needed for
	// actual bits; get extra to hold size.
	p = malloc (sizeof (int)*(ints_needed + 1));
	if (p == NULL)	return NULL;
	p->size = size;					// record bit count
	for (k=0; k<ints_needed; k++)	p->bits[k] = 0;
	return p;
}

void printbitarray (pBitArray p)
{
	int		i, k, bitcount, bitsleft, intndx;
	unsigned int mask;
	if (p == NULL)	return;

	bitcount = p->size;			// total bits to show
	bitsleft = WORDSZ;			// bits to show in current integer
	intndx = 0;					// index into int array
	mask = 1;					// mask to isolate bits

	while (bitcount > 0)	{
		// print 1 or 0 for masked bit in current integer
		putchar ((p->bits[intndx] & mask) ? '1' : '0');
		// adjust mask
		mask = mask<<1;
		// one less bit to show
		bitcount--; bitsleft--;
		// Did we fall off an integer?
		if (bitsleft == 0)	{
			intndx++;
			bitsleft = WORDSZ;
			mask = 1;
			// Make output easier to read
			putchar (' ');
			}
		}
	putchar ('\n');
}
		
#if 0
void printbitarray (pBitArray p)
{
	int i, count;
	if (p == NULL)	return;

	for (i=0; i<p->size/WORDSZ + 1; i++)	{
		printf ("Index: %d Value: %08x\n", i, p->bits[i]);
		}
	for (i=0; i<p->size/WORDSZ + 1; i++)	{
		int		k, mask;

		for (mask=1,k=0; k<WORDSZ; k++,mask = mask<<1)	{
			putchar ((p->bits[i] & mask) ? '1' : '0');
			}
		putchar (' ');
		}
	putchar ('\n');
}
#endif

int setbit (pBitArray p, int bitno)
{
	int		ndx, bitposn;
	if (p == NULL || bitno >= p->size)	return -1;
	if (SHOW)
		printf ("Index: %d  Mask: %08x\n", bitno/WORDSZ, (1<<(bitno%WORDSZ)));
	p->bits [bitno/WORDSZ]  |=  (1<<(bitno%WORDSZ));
	return 0;
}

int clrbit (pBitArray p, int bitno)
{
	int		ndx, bitposn;
	if (p == NULL || bitno >= p->size)	return -1;
	p->bits [bitno/WORDSZ] &=  ~(1<<(bitno%WORDSZ));
	return 0;
}

int tstbit (pBitArray p, int bitno)
{
	int		ndx, bitposn;
	if (p == NULL || bitno >= p->size)	return -1;
	return p->bits[bitno/WORDSZ] & (1<<(bitno%WORDSZ));
}



int main (void)
{
    int     	i, k;
	pBitArray	pba;

	pba = initbitarray (70);
	setbit (pba, 32);
	printbitarray (pba);
	setbit (pba, 31);
	printbitarray (pba);
	clrbit (pba, 32);
	clrbit (pba, 31);
	setbit (pba, 45);
	printbitarray (pba);
	for (i=60; i<68; i++)	setbit (pba, i);
	printbitarray (pba);
	for (i=68; i>=60; i -=2)	clrbit (pba, i);
	printbitarray (pba);
	for (i=60; i<68; i++)	clrbit (pba, i);
	clrbit (pba, 45);
	printbitarray (pba);
	for (i=30; i<66; i++)	{setbit (pba, i);  printbitarray (pba);}
    return 0;
}
