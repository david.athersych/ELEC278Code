// BIGOEXAMPLE.C

#include <stdio.h>

int   g;   // global variable to hold count


int fun (int n)
{
    g++;
    if (n <= 1)   return 1;
    return 1 + fun (n-1);
}


int main (void)
{
    int  k, m;

    for (k=20;  k>0;  k--)  {
        g = 0;
        m = fun (k);
        printf ("Use  %2d   answer is:   %2d   Calls to fun:  %2d\n", k, m, g);
        }
    return 0;
}
