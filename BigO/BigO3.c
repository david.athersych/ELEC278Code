#include <stdio.h>

int g = 0;


int func(int n)
{
    int    i;
    int    x = 0;
    g++;
	if (n <= 1)	return 1;
    // Interesting - a good compiler replaces this with x = n
	for (i = 0; i < n; i++)     x++;
    return  x+func(n / 2);
}

int main(void)
{
    int   m, ans;
    for (m=0;  m<20; m++)	{
		g = 0;
		ans = func (m);
		printf ("For:  %3d   Answer:  %3d    Calls:  %3d\n", m, ans, g);
		}
    return 0;
}

