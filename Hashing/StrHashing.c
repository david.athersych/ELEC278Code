// STRHASHING.C  [181103]
// Code to illustrate hashing with string keys.
//
// The premise is that we're keeping track of records based on the name
// of a person - last name and first name.  Because people sometimes have
// identical names, we also added home street name, because it is (we guess)
// unlikely that two same-named people live on the same street.
//
// We form a reduced key by taking the first 9 letters of the last name,
// first 7 letters of the first name and first 9 letters of the street name.
// If a particular piece of data has fewer characters in one or more fields,
// we pad by repeating characters from the beginning of the short word.
//
// THIS CODE IS INCOMPLETE. IT IS AN EXERCISE FOR THE STUDENT (EFTS) TO
// MAKE WHATEVER CHANGES ARE NECESSARY TO ADD THE MISSING FUNCTIONALITY.
// IN PARTICULAR, THIS CODE:
//	Doesn't do automatic resize yet
//	Doesn't handle delete or lookup yet.


#include <stdio.h>
#define	MAXBUF	128			// size of input text buffer for reading data
#define	KEYBUF	32			// size of buffer to hold condensed key

// Manifests:
// Define TESTING to include main function used to test this code.
// Define HEADER to include code that would normally be in header
// file (extracted from this file by MKHDR).

#define HEADER
//#define	TESTING		// actually done on compiler command line

#ifdef HEADER
// Inserted as part of C program template.  Everything between the ifdef
// and the endif will be extracted into a header file (a .h file) by the
// MKHEADER program.

#endif //HEADER


#ifdef TESTING
// This code used to test the hash code.

// Useful string manipulation code
#include "cstrutil_lite.c"

// Messages to the user.

char	*info[3]	=	{
		"Program to demonstrate basic hashing application using",
		"strings as keys.",
		NULL			};

char	*menu[6]	=	{
		"=====================================================",
		"1)Create new hash table          4)Insert file",
        "2)Resize existing hash table     5)Print table:",
		"3)Insert one element             6)Quit",
        "\nEnter Your Choice:  ",
		NULL			};


int largerprime (int n)
// Finds next prime number same or larger than n.
{
	static int primes [40] =	{ 19,
			  23,   89,  167,  251,  347,  433,  523,  619,  727,  827,
			 937, 1033, 1129, 1249, 1367, 1481, 1529, 1693, 1801, 1931,
			2039, 2143, 2281, 2383, 2521, 2657, 2731, 2851, 2971, 3119,
			3253, 3361, 3499, 3607, 3719, 3851, 3967, 4093, 4229
			};
	int		rslt, top = 39, bottom = 1, mid, candidate;

	for (;;)	{
		mid = (bottom + top) >> 1;			// Index of half way point
		candidate = primes[mid];
		if (candidate == n)	return n;		// Actual prime given
		if (candidate > n)	{
			if (primes[mid-1] < n)	return candidate;
			top = mid;
		} else	{
			if (primes[mid+1] > n)	return primes[mid+1];
			bottom = mid;
			}
		}
}//largerprime()

// For routines returning an integer status. Uses typical UNIX/LINUX
// convention that non-zero is error and 0 is OK; opposite to WINDOWS
// convention - and thus earns the author an "old f*rt" label in some
// CodeProject forums.
#define	HTERROR		(-1)
#define	HTOK		(0)

typedef enum _status {EMPTY=0, OCCUPIED, REMOVED} Status;
typedef unsigned int	hashcode;

hashcode sdbm(unsigned char *str);


typedef struct _hashtableentry	{
	char		*key;			// key string (pointer to it)
	char		*fulldata;		// original string (pointer to it)
	hashcode	hash;			// keep actual hash code for key
	Status		stat;			// status of this table slot
	} HashTableEntry, *pHashTableEntry;
#define	SZHTENTRY	(sizeof (struct _hashtableentry))


typedef struct _hashtable	{
	int				size;		// size of array
	int				used;		// slots actually in use
	HashTableEntry	*parray;	// pointer to actual array
	} HashTable;


hashcode sdbm(unsigned char *str);



HashTable *newHashTable(int sz)
// Create new hash table with suggested size of sz. Actual size will be next
// larger prime number.
{
	HashTable	*p = (HashTable *)malloc(sizeof (struct _hashtable));
	if (p != NULL)	{
		// got memory for hash table descriptor - do necessary init.
		if (sz <= 0)	{
			p->size = 0;
			p->used = 0;
			p->parray = NULL;
		} else	{
			p->size = largerprime (sz);
			p->used = 0;
			// Allocate block of memory preset to 0. Initial values of
			// pointers are NULL and initial value of stat is EMPTY.
			p->parray = (HashTableEntry *)calloc(p->size, SZHTENTRY);
			if (p->parray == NULL)	{
				free (p);
				p = NULL;
				}
			}
		}
	return p;
}//newHashTable()


void destroyHashTable (HashTable *pHT)
// Free actual hash table data and free descriptor memory. Caller must not
// use the hash table pointer any more.
// Note to student: Could we redesign this interface to prevent any
// "use-after-free" (UAF) errors?
{
	if (pHT != NULL)	{
		if (pHT->parray !=NULL)	free (pHT->parray);
		free (pHT);
		}
}//destroyHashTable()


int hashtoindex (hashcode hash, HashTable *pHT)
// Take hash key and create index into hash table.  This is the step where
// the current size of the table is taken into account.
{
	return (pHT->size > 0) ? hash % pHT->size : HTERROR;
}//hashtoindex()


int insertelement (HashTable *pHT, char *key, char *full, hashcode hash)
// Insert element into hash table. Parameter key points to the actual data
// (just a key in this example); parameter hash is the code computed from
// the key.
{
	int		index, rslt = HTERROR;
	printf ("  %% Inserting %s (%s) based on %u\n",
				key, full, hash);
	if (pHT != NULL)	{
		if (pHT->used == pHT->size)	{
			printf ("ERROR - Table full\n");
		} else	{
			index = hashtoindex (hash, pHT);
			printf ("  %%First choice is index %d\n", index);
			if (index != HTERROR)	{
				HashTableEntry *a = pHT->parray;
				while (a[index].stat == OCCUPIED)	{
					printf ("  %% %d already used, ", index);
					if (++index >= pHT->size)	index = 0;
					printf (" try %d, ", index);
					}
				printf ("use %d\n", index);
				a[index].key = key;
				a[index].fulldata = full;
				a[index].hash = hash;
				a[index].stat = OCCUPIED;
				pHT->used++;
				rslt = index;
				}
			}
		// What we should do here is look at load factor, and if greater
		// than some threshold, rebuild table.
		// printf ("\tSize: %d  Used: %d\n", pHT->size, pHT->used);
		}
	return rslt;		
}


void process_raw_input (HashTable *pHT, char *buffer)
{
	char		*pdata;		// pointer to local copy of data
	char		**vect;		// holds pointers to words in original buffer
	char		*pkeybuf;	// points to condensed key
	int			count;		// check number of words
	hashcode	hash;		// hashcode for condensed key
	int			index;		// index into hashtable array
	// save copy of raw data
	pdata = strdup (buffer);
	// Now split data into words. Returns vector pointing to words found
	// in original string.
	vect = bldvect (buffer, "\t\n, ", &count);
	if (count != 3)	{
		printf ("ERROR - Supposed to be 3 words\n");
		}
	// Now create condensed key
	pkeybuf = (char *)malloc(KEYBUF);
	copyandpad (vect[0], pkeybuf,    9);
	copyandpad (vect[1], pkeybuf+ 9, 7);
	copyandpad (vect[2], pkeybuf+16, 9);
	pkeybuf[25] = CH_NUL;
	printf ("Condensed key: %s", pkeybuf);
	// compute hash code
	hash = sdbm (pkeybuf);
	printf (" -- Hash code: %u\n", hash);
	index = insertelement (pHT, pkeybuf, pdata, hash);
	printf (" -- Index: %d\n", index);
	printf ("\tSize: %d  Used: %d\n", pHT->size, pHT->used);
}//process_raw_input()


HashTable *growHashTable (HashTable *pHT)
// Grow an existing hashtable. Returns pointer to new hash table with
// existing entries in their correct locations, or NULL.  Take possibility
// of NULL return into account when calling this code.
{
	HashTable		*newpHT;
	HashTableEntry	*phte;
	int				i, newindex, rslt;

	// First - create new hashtable with size approximately double existing
	// one.
	if ((newpHT = newHashTable (pHT->size * 2)) == NULL)	{
		// oops - couldn't create a new one.
		return newpHT;
		}
	// OK - got a new table. Go through existing table and place entries
	// into new one.
	phte = pHT->parray;			// pointer to existing array
	// go through existing array
	for (i=0; i<pHT->size; i++)	{
		if (phte[i].stat == OCCUPIED)	{
			// Only concerned with elements containing data. Note that we
			// kept actual hash code, so we just have to compute index in
			// new array.
			rslt = insertelement (newpHT, phte[i].key,
								phte[i].fulldata, phte[i].hash);
			if (rslt == HTERROR)	{
				// something went wrong
				destroyHashTable (newpHT);
				newpHT = NULL;
				break;
				}
			}
		}
	return newpHT;
}//growHashTable()


void PrintHashTable (HashTable *pHT, int all)
{
	HashTableEntry	*phte = pHT->parray;
	int				i;

	printf (" ** Table Contents **\n");
	printf ("   Table size: %d  actually used: %d\n",
			pHT->size, pHT->used); 
	for (i=0; i<pHT->size; i++)	{
		if (all || phte[i].stat != EMPTY)	{
			printf ("%3d: (%d)", i, (int)phte[i].stat);
			fflush (stdout);
			}
		switch (phte[i].stat)	{
		case EMPTY:
			if (all)
				printf ("MT\n");
			break;
		case OCCUPIED:
			printf ("OCC");
			goto print_remainder;
		case REMOVED:
			printf ("REM");
		print_remainder:
			printf ("  %12u (%s) %s\n", phte[i].hash, phte[i].key, phte[i].fulldata);
			break;
		default:
			printf ("WTF  %12u  %s\n", phte[i].hash, phte[i].fulldata);
			break;
			}
		}
}//PrintHashTable()


int loadfactor (HashTable *pHT)
// Open address hash table load factor calculation. Returns (truncated)
// integer percent table occupied (or -1 if table not yet allocated)
{
	return (pHT->size > 0) ?(pHT->used*100)/pHT->size : -1;
}//loadfactor()


// Smewhat clumsy method of selecting which string hashing method we're using.
// Can you suggest a better method - maybe even one that uses a command-line
// parameter?

//#define METHOD_1	1
#define	METHOD_2	1

hashcode sdbm(unsigned char *str)
{
#ifdef	METHOD_1
	// Another hash function found to perform well. Used in some open-source
	// database tools. Documentation suggests that 65599 was simply a guess
	// that proved to work satisfactorily.
    hashcode	hash = 0;
    int c;
    while (c = *str++)
        hash = c + (hash << 6) + (hash << 16) - hash;   // c + hash*65599(prime)
    return hash;
#endif
#ifdef METHOD_2
	hashcode	hash = 5381;
    int c;
    while (c = *str++)
        hash = ((hash << 5) + hash) + c;  // hash * 33 + c
    return hash;
#endif
}//sdbm()


int main(int argc, char *argv[])
{
	int			op, bad, count, index, i;
	int			sz, newsz;
	char		buffer [MAXBUF];
	HashTable	*pHT = NULL;
	hashcode	hash;
	FILE		*fp;		

	printf ("%s:\n", cmdname (argv[0]));
	show_strings (info, LEAD_TAB|TRAIL_NL);
    do	{
        show_strings (menu, LEAD_NL);
        scanf("%d",&op);
		while (getchar() != '\n');
        
        switch(op)	{
        case 1:		// Create a new hashtable
			bad = 1;
			do	{
				printf("Enter table size ( 20 <= N <= 4000):");
				scanf("%d",&sz);
				bad = (sz < 20 || sz > 4000);
				if (bad)	printf ("Error: size out of range\n");
			} while (bad);

			printf ("Closest prime: %d\n", largerprime (sz));
			pHT = newHashTable (sz);
			printf ("Hash table size: %d\n", pHT->size);
			break;
		case 2:		// Resize existing hash table
			if (pHT == NULL)	{
				printf ("ERROR - No hash table to resize!\n");
			} else	{
				pHT = growHashTable (pHT);
				}
			printf ("New Hash table size: %d\n", pHT->size);
			break;
        case 3:		// Enter new data item
			printf("Enter data - lastname, firstname, street: ");
			fgets (buffer, MAXBUF, stdin);
			stripNL (buffer);
			process_raw_input (pHT, buffer);
			break;

		case 4:
			if (pHT == NULL)	{
				printf ("ERROR: Create table first\n");
				break;
				}
			printf ("Enter file name: ");
			fgets (buffer, MAXBUF, stdin);
			stripNL (buffer);
			printf ("Input file: %s\n", buffer);
			fp = fopen (buffer, "r");
			if (fp != NULL)	{
				while (!feof (fp))	{
					if (fgets (buffer, MAXBUF, fp) == NULL)	break;
					stripNL (buffer);
					printf ("Input: %s\n", buffer);
					process_raw_input (pHT, buffer);
					}
				fclose (fp);
			} else	{
				printf ("ERROR - No file\n");
				}
			printf ("Completed file\n");
			break;

		case 5:		// Print table
			PrintHashTable (pHT, 0);
			break;
 
		}
    } while(op!=6);
  
	return 0;
}// main()

#endif //TESTING
