// SINHASH [161103]
// Code to implement various hash codes for Canadian social insurance
// numbers.

#include <stdio.h>


#define  odd(x)	(x & 1)

typedef unsigned int	uint;


uint hash_functionA (uint sin)
// Simple one to add the even digits plus double the odd digits.
// Note the maximum is 5*9 plus 4*2*9 = 117.
{
	uint	answer = 0;
	int		i;
	for (i=0; i<9; i++)	{
		answer = answer + (odd(i)?2:1)*(sin%10);
		sin = sin/10;
		}
	return answer;
}// hash_functionA()


uint hash_functionB (uint sin)
// Get rid of top two digits and 3 bottom digits - ie pull out middle 4
// digits.  Maximum here is 9999.
{
	// First, get rid of top 2 digits
	sin = sin % 10000000;
	// Then, get rid of botton 3 digits
	sin = sin / 1000;
	return sin;
}//hash_functionB()

// helper functions - trust compiler to make even more efficient

uint mul_91(uint d) { return d + (d<<1) + (d<<3) + (d<<4) + (d<<6); }

uint mul_197(uint d) { return d + (d<<2) + (d<<6) + (d<<7); }

uint mul_487(uint d) { return d + (d<<2) + (d<<5) + (d<<6) + (d<<7) + (d<<8); }


uint hash_functionC (uint sin)
// Implement more elaborate hash function that either of first two.
{
	uint	answer = 0;
	uint	digit;
	for (int i = 1; i<10; i++)	{
		digit = sin % 10;
		switch (i)	{
		case 2:		answer += digit;   break;
		case 1:
		case 3:
		case 5:		answer += mul_197(digit);  break;
		case 4:
		case 6:
		case 8:		answer += mul_91(digit);   break;
		case 7:
		case 9:		answer += mul_487(digit);  break;
			}//endswitch
		}//endfor

	return answer;
}//hash_functionC()



//#define TEST 1

int main(void)
{
	uint	sin;
    uint	hashvalue;
	char	buff [128];

#ifdef TEST
	for (uint i = 0; i < 5; i++)
		printf ("%d:  %4d %6d %7d\n", i, mul_91(i), mul_197(i), mul_487(i));
#else
	while (1)	{
		printf ("Enter 9 digit number: ");
		gets (buff);
		sin = atoi(buff);
		if (sin == 0)	break;
		if (sin > 100000000  && sin < 999999999)	{
			// valid number
			printf ("Hash values: (A) %d   (B) %d  (C) %d\n",
						hash_functionA(sin)%1000,
						hash_functionB(sin)%1000,
						hash_functionC(sin)%1000);
		} else	{
			printf ("Illegal number\n");
			}
		}
#endif
	return 0;
}
