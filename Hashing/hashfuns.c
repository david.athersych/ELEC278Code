unsigned long hash_djb2(unsigned char *str) 
{
    unsigned long hash = 5381;
    int c;
    while (c = *str++)
        hash = ((hash << 5) + hash) + c;  // hash * 33 + c
    return hash;
}//hash_djb2()


unsigned long hash_sdbm(unsigned char *str) 
{
    unsigned long hash = 0;
    int c;
    while (c = *str++)
        hash = c + (hash << 6) + (hash << 16) - hash;   // c + hash*65599(prime)
    return hash;
}//hash_sdbm()
