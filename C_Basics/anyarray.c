// Anyarray.c
#include <stdio.h>

int  matrix [3][3];

int main(void)
{
    int    i, k;
    for (i=0; i<9; i++)  matrix [i] = i;

    for (i=0; i<3; i++)
        for (k=0; k<3; k++)
            printf( "matrix[%d][%d] is: %d\n", i, k, matrix[i][k]);
    return 0;
}
