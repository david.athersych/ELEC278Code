// ARGS.C [940327]
// Test command just shows its arguments and the environment
// Usage:
//       args arg1 arg2 arg3
// will print args arg1 arg2 arg3 - whatever you type
// Try it as:     args "arg1 arg2" arg3 and see what it says
// What's the difference when you run this on Windows and Linux?

#include	<stdlib.h>
#include	<stdio.h>

void main (int argc, char **argv, char **environ)
{
	int		n;
	char	*p;

	// print out argument list for this program
	printf("\nPROGRAM ARGUMENTS:\n");
	for (n=0; n<argc; n++) {
		printf("[%2d]  %s\n", n, argv[n]);
		}

	// Now print out environment variables. We do not have
	// a count; have to look for NULL pointer at end of vector
	// (Yes, you can make the following code more compact.)
	printf ("\n\nPROGRAM ENVIRONMENT:\n");
	n = 0;
	while (environ[n] != NULL)	{
        printf ("[%3d]  %s\n", n, environ[n]);
        n++;
        }
} //main()
