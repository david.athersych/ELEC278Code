// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

int addem(int x, int y)
{
	return x+y;
}

int subem(int x, int y)
{
	return x-y;
}

int main(int argc, char* argv[])
{
	int (*f)(int,int);
	int a, b, c;

	a = 20;
	b = 30;
	f = addem;
	c = (*f)(a,b);
	printf ("%d %d %d\n", a, b, c);
	a = 20;
	b = 30;
	c = f(a,b);
	printf ("%d %d %d\n", a, b, c);
	a = 20;
	b = 30;
	f = subem;
	c = (*f)(a,b);
	printf ("%d %d %d\n", a, b, c);
	a = 20;
	b = 30;
	c = f(a,b);
	printf ("%d %d %d\n", a, b, c);
	return 0;
}

