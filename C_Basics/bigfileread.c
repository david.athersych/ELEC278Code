// BIGFILEREAD.C [161117]
// Program to read a test file of random records, used to test code
// written for the tutorial assignment on November 15, 2016.

/*
Original problem statement:

You have a collection of data records stored in a file that is too big
to store in memory. Each record of data has a 28 character key associated
with it.
The items are stored in the file in no particular order. You are asked to
write code that will create a sorted version of the file.  You are told
that each record is between 1 and 3 megabytes in size, and that the first
two fields in each record are (1) the 28 byte key with no trailing NUL
character and (2) a 4 byte integer indicating the size of the record,
including the key and size fields.  Show your code.

*/


#include <stdio.h>
#include <stdlib.h>

int		lastreaderror = 0;

int read_one_record (FILE *fp)
// Read one record to already-opened output file.
{
	long int	number;
	int			size;
	char		buff [30];
	int			rslt;

	// Step 1. Read 28 byte key
	rslt = fread (buff, 1, 28, fp);
	if (rslt != 28)	{
		lastreaderror = 1;		// fail to read key
		return 0;
		}
	buff [28] = '\0';
	printf ("Key: %s, ", buff);
	rslt = fread (&size, sizeof (int), 1, fp);
	if (rslt != 1)	{
		lastreaderror = 2;		// fail to read size
		return 0;
		}
	printf ("Size: %d\n", size);
	size -= 32;					// remainder to skip
	fseek (fp, size, SEEK_CUR);
	return 1;
}


int main (int argc, char *argv[])
{
	int		numrecords;
	int		i;
	int		rslt;
	FILE	*fpinn;


	printf ("Read file of random records\n");

	// open input file (binary mode because this is Windoze
	fpinn = fopen ("RANDOM.DAT", "rb");
	if (fpinn == NULL)	{
		printf ("ERROR - Failed to open input file\n");
		return -1;
		}
	while (!feof(fpinn) && read_one_record (fpinn));
	
	fclose (fpinn);
	return 0;
}

	
