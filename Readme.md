Source code used for teaching purposes in:
ELEC278, Queen's University, Kingston, Fall semester 2019.
This code is provided without warranty of any kind.  It is the
responsibility of the user to determine the correctness and the
usefulness of this code for any purpose.

Author:  David F. Athersych, P.Eng.
All rights reserved. This code is intended for students
registered in ELEC278 for the semester listed above.

See LICENCE.MD for restrictions on the use of this code.
