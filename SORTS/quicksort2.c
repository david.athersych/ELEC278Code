// QUICKSORT [161114]
//
// Implements simple quicksort algorithm.  Separate partition functions
// are provided for different choices of pivot element.

#include <stdlib.h>
#include <stdio.h>

typedef int		T;


T startdata[] = {19, 11, 93, 42, 67, 18, 13, 48, 6, 7, 38, 27, 22, 12, 5};
T data[100];
int datasize = 20;

void makedata (void)
{
	static int	init = 0;
	int		i;
	if (init == 0)	{
		srand (140356789367);
		init = 1;
		}
	for (i=0; i<datasize; i++)	data[i] = rand ();
}


void printarray(T *a, int size)
{
    int i;
    for (i=0; i < size; i++)	printf(" %2d", a[i]);
    putchar('\n');
}

void printarrowatposition (int posn)
{
	int	i;
	for (i=0; i<posn; i++)		printf ("   ");
	printf ("  V\n");
}


void swap (T *a, int g, int h)
{
	int  t = a[g];
	printf ("   Swap posn %d (%d) with posn %d (%d)\n", g, a[g], h, a[h]);
	a[g] = a[h];
	a[h] = t;
}


int partition_left_pivot (T  *a,  int left, int right)
// Partition portion of array a between index left and index right, using
// leftmost value as pivot value.
{
	int	ll, rr, pivotval;
	pivotval = a[left];
	printf ("Partition around value in position %d - %d\n", left, pivotval);
	// Set up indices for left and right scans
	ll=left+1;
	rr=right;
	
    while (ll < rr)	{
		// if one on left less than pivot, leave it
		if(a[ll] <= pivotval)	{ ll++; continue;}
		// if one on right greater than pivot, leave it
        if(a[rr] >= pivotval)	{ rr--; continue;}
		// both left and right on wrong side - swap them
		swap(a, ll, rr);
       	}//endwhile
	// we stop when rr and ll collide. Place pivot value such that everything
	// to left is less and everything to right is same or greater.
	if (a[ll] < pivotval)	swap(a, ll, left);
	else					swap(a, --ll, left);
	return ll;
}//partition_left_pivot()


int partition_right_pivot (T  *a,  int left, int right)
// Partition portion of array a between index left and index right, using
// rightmost value as pivot value.
{
	int	ll, rr, pivotval;
	pivotval = a[right];
	printf ("Partition around value in position %d - %d\n", right, pivotval);
	// Set up start points for left and right scans
	ll=left;
	rr=right-1;
	
    while (ll < rr)	{
		// if one on left less than pivot, leave it
		if(a[ll] <= pivotval)	{ ll++; continue;}
		// if one on right greater than pivot, leave it
        if(a[rr] >= pivotval)	{ rr--; continue;}
		// both left and right on wrong side - swap them
		swap(a, ll, rr);
       	}//endwhile
	// we stop when rr and ll collide. Place pivot value such that everything
	// to left is less and everything to right is same or greater.
	if (a[rr] > pivotval)	swap(a, rr, right);
	else					swap(a, ++rr, right);
	return rr;
}//partition_right_pivot()


int partition_middle_pivot (T  *a,  int left, int right)
// Partition portion of array a between index left and index right, using
// middle value as pivot value.
{
	int	ll, rr, pivotval, mid;
	// Compute index of middle of array
	ll = left;
	rr = right;
	mid = (ll+rr)/2;
	pivotval = a[mid];
	printf ("Partition around value in position %d - %d\n", mid, pivotval);
	
    while ((ll < mid) && (rr > mid))	{
		if (a[ll] <= pivotval)	{ ll++; continue; }
		if (a[rr] >= pivotval)	{ rr--; continue; }
		// both left and right on wrong side - swap them
		swap(a, ll, rr);
       	}//endwhile
printf ("Finished first step: mid = %d, ll = %d, rr = %d\n", mid, ll, rr);
	// Stop when either ll or rr hits mid. This doesn't mean we're done. There
	// may be a few elements in other partition out of place.
	if (ll==mid && rr==mid)	return mid;
	if (rr > mid)	{
		// may need to shuffle pivot to right a bit
		ll = mid + 1;
		}
	if (ll < mid)	{
		// may need to shuffle pivot to left a bit
		rr = mid - 1;
		}
printf ("Finished second step: mid = %d, ll = %d, rr = %d\n", mid, ll, rr);

	// ll may now be in what was the right partition, or rr may be in what was
	// left parition.  Keep scanning and swapping when necessary.
	while (ll < rr)	{
		if (a[11] <= pivotval)	{ ll++; continue; }
		if (a[rr] >= pivotval)	{ rr--; continue; }
		// two need to be swapped
		swap (a, ll, rr);
		}
printf ("Finished third step: mid = %d, ll = %d, rr = %d\n", mid, ll, rr);

	// OK got two partitions, 
	while (ll > left && a[ll] > pivotval)	ll--;
	swap (a, mid, ll);
	mid = ll;
	return mid;
}//partition_middle_pivot()


int partition_better_middle_pivot (T  *a,  int left, int right)
// Partition portion of array a between index left and index right, using
// middle value as pivot value.
{
	int	pivotval, middle;
	// Compute index of middle of array
	middle = (left+right)/2;
	pivotval = a[middle];
	printf ("Partition around value in position %d - %d\n", middle, pivotval);
	swap (a, middle, left);
	return partition_left_pivot (a, left, right);
}//partition_better_middle_pivot()



void quicksort (T  *a,  int left, int right, int (*partition)(T *a, int left, int right))
// Plain quicksort code, that takes partition down to trivial size.
// Note use of function pointer as parameter, so several partition routines can be tested.
{
	printf ("Quicksort: left = %d   right = %d\n", left, right);
	if (left < right)    {
		if (left+1 == right)	{
			// Trivial sort
			if (a[left] > a[right])	swap (a, left, right);
		} else	{
			int  pivotndx = partition (a, left, right);
			printf ("    Completed partition, pivot at: %d\n", pivotndx);
			printarrowatposition (pivotndx);
			printarray (data, datasize);
			quicksort (a, left, pivotndx-1, partition);
			quicksort (a, pivotndx+1, right, partition);
			}
		}
} 


int main(void)
{
	int		i;
	//for (i=0; i<datasize; i++)	data[i] = startdata[i];
	makedata ();
	printf ("Show operation of QUICKSORT using leftmost pivot\n");
    printf("Original array: \n");
    printarray(data, datasize);
    quicksort(data, 0, datasize-1, partition_left_pivot);
    printf("\nSorted array: \n");
    printarray(data, datasize);
	getchar ();
	//for (i=0; i<datasize; i++)	data[i] = startdata[i];
	makedata ();
	printf ("Show operation of QUICKSORT using rightmost pivot\n");
    printf("Original array: \n");
    printarray(data, datasize);
    quicksort(data, 0, datasize-1, partition_right_pivot);
    printf("\nSorted array: \n");
    printarray(data, datasize);
	getchar ();
	//for (i=0; i<datasize; i++)	data[i] = startdata[i];
	makedata();
	printf ("Show operation of QUICKSORT using middle pivot (awkward version)\n");
    printf("Original array: \n");
    printarray(data, datasize);
    quicksort(data, 0, datasize-1, partition_middle_pivot);
    printf("\nSorted array: \n");
    printarray(data, datasize);
	getchar ();
	//for (i=0; i<datasize; i++)	data[i] = startdata[i];
	makedata ();
	printf ("Show operation of QUICKSORT using middle pivot (better version)\n");
    printf("Original array: \n");
    printarray(data, datasize);
    quicksort(data, 0, datasize-1, partition_better_middle_pivot);
    printf("\nSorted array: \n");
    printarray(data, datasize);
	getchar ();
    return 0;
}
