// SELSORT [161111]
// Simple selection sort

#include <stdio.h>

int		a[100] = {30, 10, 91, 3, 5, 22, 37, 18, 4, 43};
int		length = 10;

void printdata (void)
{
	int		i;
	for (i=0; i<length; i++)	printf (" %3d", a[i]);
	putchar ('\n');
}

void swap (int *d, int g, int h)
{
	int	t = d[g];
	printf ("Swap elements at %d (%d) and %d (%d)\n", g,d[g],h,d[h]);
	d[g] = d[h];   d[h] = t;
}

int findbiggest (int *pd, int min, int max)
{
	int		i;
	int		bigndx;

	// Start by assuming biggest value is in first place
	bigndx = min;

	// Now check all other places
	for (i=min+1; i<=max; i++)
		if (pd[i] > pd[bigndx])	{
			// Data at i bigger than biggest so far
			bigndx = i;
			}
	// Checked everywhere
	return bigndx;
}//findbiggest()


int		callcounter = 0;

void selsort (int *pd, int min, int max)
{
	int		bigplace;
	int		temp;
	callcounter++;
	if (min >= max)	return;
	bigplace = findbiggest (pd, min, max);
	swap (pd, bigplace,max);
	printf ("Call #%d:      ", callcounter);
	printdata ();
	selsort (pd, min, max-1);
}//selsort()


int main (void)
{
	printf ("Initial data: ");
	printdata ();
	selsort (a, 0, length-1);
	printf ("Final array:  ");
	printdata ();
	return 0;
}//main()
	
