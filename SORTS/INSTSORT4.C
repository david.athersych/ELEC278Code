// INSTSORT4 [171109]
//
// INSERTION SORT - simple version using integers
// Version 4 - use array to point to defined data type


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define	TALKATIVE	0


//=============================================================================

struct student   {
	char	*name;
	unsigned int	stnumber;
	int		mark;
};

#define	SZSTUDENT	(sizeof(struct student))


// Constructor, destructor for student structure
struct student *create_student (char *n, unsigned int no, int mk)
{
	struct student *p = (struct student *)malloc (SZSTUDENT);
	if (p)	{
		p->name = strdup(n);
		p->stnumber = no;
		p->mark = mk;
	}
	return p;
}

void destroy_student (struct student *p)
{
	if (p)	{
		if (p->name)	free (p->name);
		free (p);
	}
}

// Compare two structures, based on one of their fields
int compare_student_by_name (struct student *a, struct student *b)
{
	return strcmp(a->name, b->name);
}

int compare_student_by_mark (struct student *a, struct student *b)
{
	return (a->mark - b->mark);
}

int compare_student_by_number (struct student *a, struct student *b)
{
	return (int)a->stnumber - (int)b->stnumber;
}

//=============================================================================

#define	MAXLEN	100

typedef struct student	T;




void printdata (T **data, int maxndx)
{
	int		i;
	// more detailed - show index numbers as well as data
	for (i=0; i<maxndx; i++)	{
		printf ("%3d: %-20s %9d %4d\n", i, data[i]->name, data[i]->stnumber, data[i]->mark);
		}
}// printdata


int insert (T *new,  T **data, int currndx, int maxlen, int (*cmp)(T*, T*) )
// Insert new value into data array, in correct place.  Currndx is current largest index; maxlen is maximum length
// of array.  Cmp is pointer to a function that compares two items of type T.
{
    int		i;
    char	*ps;

    // Array full? Don't try inserting!
    if (currndx >= maxlen-1)	return -1;

	if (TALKATIVE)
	    printf ("About to insert %s (used: %d)\n", new->name, currndx);

    // Start by placing (pointer to) new item at end. Note that length increases by 1
    data [currndx++] = new;
    // Now shuffle new value up - note starting value for i.
    for (i=currndx-1; i>0; i--)	{
        // compare strings pointed to by array values, but just move pointers when out of order
        if (cmp (data[i],data[i-1]) < 0)	{
            // data in larger index is smaller than data in previous index � need to swap
            T *tmp = data [i];  data [i] = data [i-1];  data [i-1] = tmp;
			if (TALKATIVE)   printf ("Switched %d and %d\n", i, i-1);
        } else     // array was sorted before add, so done when no swap
            break;
        }
    return currndx;
}//insert()




int main(int argc, char *argv[])
{
	struct student *data [MAXLEN];	// Note array of pointers
	struct student *byname [MAXLEN];
	struct student *bystnum [MAXLEN];
	struct student *bymark [MAXLEN];
	int	len = 0;		// current length of raw data
	int	i;
	int	lenbyname = 0;	// current length data by name
	int lenbystnum = 0;	// current lenght data by student number
	int lenbymark = 0;	// current length data by mark
	int	rslt;

	// Build raw data
    data[len++] = create_student ("Fyfe,Barney",      10000400, 41);
	data[len++] = create_student ("Hulk,Incredible",  19994321, 12);
    data[len++] = create_student ("Woman,Wonder",     20107654, 91);
    data[len++] = create_student ("Mouse,Mickey",       127373, 65);
    data[len++] = create_student ("Hound,Huckleberry",  168799, 55);
    data[len++] = create_student ("Fudd,Elmer",       10000402, 45);
    data[len++] = create_student ("Duck,Daffy",       10000404, 35);
    data[len]   = create_student ("Bunny,Bugs",       10000406, 85);
	printf ("Raw data:\n");
	printdata (data, len);

	printf ("\n***Build sorted by name\n");
	for (i=0; i<=len; i++)	{
		printf ("Insert %s\n", data[i]->name);
		rslt = insert (data[i],byname,lenbyname,MAXLEN,compare_student_by_name);
		if (rslt == -1)	{
			printf ("Failed to insert (%d)\n", i);
			break;
		} else	{
			lenbyname = rslt;
			printdata (byname, lenbyname);
			}
		}

	printf ("\n***Build sorted by student number\n");
	for (i=0; i<=len; i++)	{
		printf ("Insert %s\n", data[i]->name);
		rslt = insert (data[i],bystnum,lenbystnum,MAXLEN,compare_student_by_number);
		if (rslt == -1)	{
			printf ("Failed to insert (%d)\n", i);
			break;
		} else	{
			lenbystnum = rslt;
			printdata (bystnum, lenbystnum);
			}
		}

	printf ("\n***Build sorted by mark\n");
	for (i=0; i<=len; i++)	{
		printf ("Insert %s\n", data[i]->name);
		rslt = insert (data[i],bymark,lenbymark,MAXLEN,compare_student_by_mark);
		if (rslt == -1)	{
			printf ("Failed to insert (%d)\n", i);
			break;
		} else	{
			lenbymark = rslt;
			printdata (bymark, lenbymark);
			}
		}
	return 0;
}
		
