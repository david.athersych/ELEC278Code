// QUICKSORTINP [161114]
//
// Implements quicksort algorithm.  Data to be sorted is embedded.
// Uses middle value for pivot.

#include <stdlib.h>
#include <stdio.h>

typedef int		T;


int data[] = {42,91,50,3,87,22,36,27,9,16,19,82,92,12,32,11,7,94,63,71};
int datasize = 20;


void printarray(T *a, int size)
{
    int i;
    for (i=0; i < size; i++)	printf(" %2d", a[i]);
    putchar('\n');
}

void printarrowatposition (int posn)
{
	int	i;
	for (i=0; i<posn; i++)		printf ("   ");
	printf ("  V\n");
}


void swap (T *a, int g, int h)
{
	int  t = a[g];
	printf ("   Swap position %d (%d) with position %d (%d)\n",
			g, a[g], h, a[h]);
	a[g] = a[h];
	a[h] = t;
}

static int indexofmedian (T *a, int left, int right)
{
	int leftval, rightval, midindex, midval;
	leftval = a[left];
	rightval = a[right];
	midindex = (left+right)/2;
	midval = a[midindex];
	if ((midval >= leftval) && (midval <= rightval))	return midindex;
	if ((midval >= rightval) && (midval <= leftval))	return midindex;
	if ((leftval >= midval) && (leftval <= rightval))	return left;
	if ((leftval >= rightval) && (leftval <= midval))	return left;
	if ((rightval >= midval) && (rightval <= leftval))	return right;
	if ((rightval >= leftval) && (rightval <= midval))	return right;
	return left;
}


int partition (T  *a,  int left, int right)
{
	int	ll, rr, pivotval, z;
	// deal with trivial cases first
	int length = right-left;
	if (length <= 0)	return left;
	if (length == 1)	{
		if (a[right] < a[left])	{
			swap (a, left, right);
			return left;
			}
		}
	if (length == 2)	{
		// for 3 items, do trivial bubble sort.
		if (a[left] > a[left+1])	swap (a, left, left+1);
		if (a[left+1] > a[right])	swap (a, left+1, right);
		if (a[left] > a[left+1])	swap (a, left, left+1);
		return left+1;
		}

	// Next, choose pivot value, and place it in left position
	z = indexofmedian (a, left, right);
	swap (a, left, z);
	ll=left+1;
	rr=right;
	pivotval = a[left];
	printf ("Partition around value in position %d - %d\n", left, a[left]);
    while (ll < rr)	{
		// if one on left less than pivot, leave it
		if(a[ll] <= pivotval)	{ ll++; continue;}
		// if one on right greater than pivot, leave it
        if(a[rr] >= pivotval)	{ rr--; continue;}
		// both left and right on wrong side - swap them
		swap(a, ll, rr);
       	}//endwhile
	// we stop when rr and ll collide. Place pivot value
	// such that everything to left is lesser and everything
	// to right is same or greater.
	if (a[ll] < pivotval)	{
		swap(a, ll, left);
	} else	{
		swap(a, --ll, left);
		}
	return ll;
}//partition()


void quicksort (T  *a,  int left, int right)
{
	printf ("\nQuicksort: left = %d   right = %d\n", left, right);
	if (left < right)    {
		int  pivotndx = partition (a, left, right);
		printf ("    Completed partition, pivot at: %d\n", pivotndx);
		printarrowatposition (pivotndx);
		printarray (data, datasize);
		quicksort (a, left, pivotndx-1);
		quicksort (a, pivotndx+1, right);
		}
} 


int main(void)
{
	printf ("Show operation of QUICKSORT\n");
    printf("Original array: \n");
    printarray(data, datasize);
 
    quicksort(data, 0, datasize-1);
 
    printf("\nSorted array: \n");
    printarray(data, datasize);
	getchar ();
    return 0;
}
