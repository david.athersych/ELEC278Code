// BFS.C
// Breadth-first search of graph
#include <stdio.h>
#include <stdlib.h>
#include "boolean.h"
typedef int	Que_data;		// create queue filled with ints
#include "quearry.h"
#define MAX 7
#include "grphbsic.h"


void breadthFirstSearch(int start)
// Do breadth-first search of global graph
{
	int	i;
	int	unvisitedVertex;
	Queue *q = create_queue (MAX+1);

	//mark first node as visited
	Vertices[start]->visited = TRUE;

	//display the vertex
	displayVertex(start);   

	//insert vertex index in queue
	insert(q, start);

	while(!isQueueEmpty(q)) {
		//get unvisited vertex of vertex which is at front of queue
		int	tempVertex;
		removefront (q, &tempVertex);

		//no adjacent vertex found
		while((unvisitedVertex = getAdjUnvisitedVertex(tempVertex)) != -1) {    
			Vertices[unvisitedVertex]->visited = TRUE;
			displayVertex(unvisitedVertex);
			insert(q, unvisitedVertex);               
			}
		}   

	printf ("\n");
	//queue is empty, search is complete, reset visited flag
	resetVisitFlags ();
	destroy_queue (&q);
}


int main (void)
{
	int		v;
	initGraph(); 

	addVertex ('A');	// 0 
	addVertex ('B');	// 1
	addVertex ('C');	// 2
	addVertex ('D');	// 3
	addVertex ('E');	// 4
	addVertex ('F');	// 5
	addVertex ('G');	// 6
 
	addDirectedEdge (0, 1);		// A - B
	addDirectedEdge (0, 2);		// A - C
	addDirectedEdge (0, 6);		// A - G
	addDirectedEdge (1, 3);		// B - D
	addDirectedEdge (2, 6);		// C - G
	addDirectedEdge (3, 4);		// D - E
	addDirectedEdge (3, 5);		// D - F
	addDirectedEdge (5, 6);		// F - G
	addDirectedEdge (5, 4);		// F - E
	
	printf ("Breadth-first searches, starting at each vertex\n");
	for (v=0; v<MAX; v++)	{
		printf("Breadth First Search starting at vertex %d:\n", v);
		breadthFirstSearch(v);
		}
	return 0;
}
