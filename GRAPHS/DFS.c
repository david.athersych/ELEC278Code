// DFS.C
// Depth first search of graph
#include <stdio.h>
#include <stdlib.h>
#include "boolean.h"
typedef int	Stk_data;
#include "StckArry.h"
#define MAX 7
#include "grphbsic.h"


void depthFirstSearch(int start)
// Do Depth first search on global graph
{
	int i;
	Stack *st = create_stack (MAX);

	//mark first node as visited
	Vertices[start]->visited = TRUE;

	//display vertex
	displayVertex(start);   

	//push vertex index onto stack
	push(st, start);

	while(!isStackEmpty(st)) {
		//get unvisited vertex of vertex which is at top of stack
		int topvertex;
		int	unvisitedVertex;
		peek (st, &topvertex);
		unvisitedVertex = getAdjUnvisitedVertex(topvertex);

		//no adjacent vertex found
		if(unvisitedVertex == -1) {
			pop(st, NULL);
		} else {
			Vertices[unvisitedVertex]->visited = TRUE;
			displayVertex(unvisitedVertex);
			push(st, unvisitedVertex);
			}
		}

	printf ("\n");
	//stack is empty, search is complete, reset all visited flags       
	resetVisitFlags ();
	destroy_stack(&st);     
}


int main (void)
{
	int		vrtx;
	initGraph();

	addVertex ('S');	// 0
	addVertex ('A');	// 1
	addVertex ('B');	// 2
	addVertex ('C');	// 3
	addVertex ('D');	// 4
	addVertex ('E');	// 5
	addVertex ('F');	// 6
 
	addEdge (0, 1);		// S - A
	addEdge (0, 2);		// S - B
	addEdge (0, 3);		// S - C
	addEdge (1, 4);		// A - D
	addEdge (2, 4);		// B - D
	addEdge (3, 4);		// C - D
	addEdge (4, 5);		// D - E
	addEdge (5, 6);		// E - F
	
	printf("Depth First Search:\n");
	for (vrtx=0; vrtx<7; vrtx++)	{
		printf ("==Start at vertex: %d\n", vrtx);
		depthFirstSearch(vrtx);
		}
	return 0;   
}
