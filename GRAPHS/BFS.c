// BFS.C
// Breadth-first search of graph
#include <stdio.h>
#include <stdlib.h>
#include "boolean.h"
typedef int	Que_data;		// create queue filled with ints
#include "quearry.h"
#define MAX 7
#include "grphbsic.h"


void breadthFirstSearch(int start)
// Do breadth-first search of global graph
{
	int	i;
	int	unvisitedVertex;
	Queue *q = create_queue (MAX+1);

	//mark first node as visited
	Vertices[start]->visited = TRUE;

	//display the vertex
	displayVertex(start);   

	//insert vertex index in queue
	insert(q, start);

	while(!isQueueEmpty(q)) {
		//get unvisited vertex of vertex which is at front of queue
		int	tempVertex;
		removefront (q, &tempVertex);

		//no adjacent vertex found
		while((unvisitedVertex = getAdjUnvisitedVertex(tempVertex)) != -1) {    
			Vertices[unvisitedVertex]->visited = TRUE;
			displayVertex(unvisitedVertex);
			insert(q, unvisitedVertex);               
			}
		}   

	printf ("\n");
	//queue is empty, search is complete, reset visited flag
	resetVisitFlags ();
	destroy_queue (&q);
}


int main (void)
{
	int		v;
	initGraph(); 

	addVertex ('S');	// 0
	addVertex ('A');	// 1 
	addVertex ('B');	// 2
	addVertex ('C');	// 3
	addVertex ('D');	// 4
	addVertex ('E');	// 5
	addVertex ('F');	// 6
 
	addEdge (0, 1);		// S - A
	addEdge (0, 2);		// S - B
	addEdge (0, 3);		// S - C
	addEdge (1, 4);		// A - D
	addEdge (2, 4);		// B - D
	addEdge (3, 4);		// C - D
	addEdge (4, 5);		// D - E
	addEdge (5, 6);		// E - F
	
	printf ("Breadth-first searches, starting at each vertex\n");
	for (v=0; v<7; v++)	{
		printf("Breadth First Search starting at vertex %d:\n", v);
		breadthFirstSearch(v);
		}
	return 0;
}
