// STCKARRY.H [160929]
// Implement simple array-based stack.

// This code is to be included in code that needs stack datatype. Prior to
// include, there must be a typedef that defines type for Stk_data.
// For example:
//	typedef int	Stk_data;		// create stack filled with ints
//	#include "stckarry.h"

#include	"boolean.h"

// ##### Implementation of simple stack of type Stk_data #####

struct stack  {
	int			tos;	// index of top-of-stack element
	int			stksz;	// maximum size of array
	Stk_data	*stk;	// pointer to array of Stk_data items
	};

typedef struct stack	Stack;

#define	MT	(-1)		// index of empty stack

Stack *create_stack (int size)
// Create stack.
{
	Stack	*s = NULL;
	// First, check for idiocy
	if (size > 0)	{
		Stk_data	*psd = (Stk_data *)malloc(size * sizeof(Stk_data));
		if (psd != NULL)	{
			// Memory for actual stack obtained; get memory for descriptor
			s = (struct stack *) malloc (sizeof (struct stack));
			if (s != NULL)	{
				// Got descriptor
				s->tos = MT;		// initially empty
				s->stksz = size;	// array size
				s->stk = psd;		// array to store stack items
				}
			}
		}
	return s;		// either pointer to stack descriptor or NULL
}//create_stack()


void destroy_stack (Stack **pps)
// Destroy stack.  Pass address of stack pointer returned from create(); this
// way we can ensure pointer is cleared as well.
{
	// always apply PAI rule.
	if (pps != NULL)	{
		Stack *ps = *pps;		// get stack descriptor
		free (ps->stk);			// free stack array
		free (ps);				// free stack descriptor
		*pps = NULL;			// zero stack descriptor tag
		}
	return;
}//destroy_stack()


BOOL isStackEmpty (Stack *s)
// Determine if stack currently empty
{
	BOOL	rslt = FALSE;
	if (s != NULL)	{
		rslt = s->tos == MT;
		}
	return rslt;
}//isStackEmpty()


BOOL isStackFull (Stack *s)
// Determine if stack is full
{
	BOOL	rslt = FALSE;
	if (s != NULL)	{
		rslt = s->tos >= (s->stksz-1);
		}
	return rslt;
}//isStackFull()


int peek (Stack *s, Stk_data *px)
// peek at top element in stack
{
	int		rslt = 0;
	// Stack pointer?
	if (s != NULL)	{
		// is there value to peek at?
		if (!isStackEmpty(s))	{
			*px = s->stk [s->tos];
			rslt = 1;				// got a value to peek at
			}
		}
	return rslt;
}//peek()


int push (Stack *s, Stk_data x)
// Push Stk_data item onto stack s. If push accomplished, returns 1.
{
	int		rslt = 0;				// return value
	// Check for valid stack
	if (s != NULL)	{
		// Check for full stack
		if (!isStackFull(s))	{
			s->tos++;				// index of free space at top of stack
			s->stk [s->tos] = x;	// copy user's data into stack slot
			rslt = 1;
			}
		}
	return rslt;
}//push()
			
	
int pop (Stack *s, Stk_data *px)
// Pop top item from stack.  If pop accomplished, returns 1 and variable
// pointed to by px holds popped value.  If stack doesn't exist or is empty,
// returns 0.
{
	int		rslt = 0;				// return value
	if (s != NULL)	{
		if (!isStackEmpty(s))	{
			// Caller want the popped value?
			if (px != NULL)
				*px = s->stk[s->tos];
			s->tos--;				// no longer occupied
			rslt = 1;
			}
		}
	return rslt;
}//pop()


// ##### End simple stack implementation #####
