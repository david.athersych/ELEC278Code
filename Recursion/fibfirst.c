// Fibonnacci calculation

#include <stdio.h>
#include <math.h>


int   g = 0;         // global counter - used by fib.



int fib (int n)
{
    // Another call - increment global counter
    g++;
    if (n<=1)  return n;
    return fib(n-1)+fib(n-2);
}




int main (void)
{
    int i, k, fibi;
    float  pw;

    printf ("   N   FIB(N) Calls to Fib  1.6^N     /g\n");
    for (i=0; i<20; i++)   {
        g = 0;

        fibi = fib (i);

        pw = pow(1.6, i);
        printf("%5d%6d%10d%12.2f%12.4f\n", i, fibi, g, pw, pw/g);
        }
    return 0;
}